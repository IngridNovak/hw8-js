"use strict";
//1.DOM це репрезентація веб сторінки, об'єктна модель документа. Створюється, коли браузер завантажує сторінку.
//Модель DOM включає вміст сторінки та елементи.Через модель DOM JS може взаємодіяти з сторінкою.
//2. innerText показує весь текстовий вміст, тобто будь-який текст,записаний між відкриваючим і закриваючим тегом.
//Всі елементи HTML всередині innerText ігноруються, повертається лише їх внутрішній текст.
//innerHTML показує текст лише по одному елементу, причому елементи HTML не ігноруються.
//3.Звернутися можна через: getElementById(),getElementByTagName(),getElementByClassName(),
//querySelectorAll(), querySelector(). Найкраще звертатися до id елемента, тобто getElementById().

let pElement = document.getElementsByTagName("p");
for(let i = 0; i < pElement.length; i++) {
    pElement[i].style.backgroundColor = '#ff0000';
}

let optionsList = document.getElementById("optionsList");
console.log(optionsList);

let parentElem = optionsList.parentElement;
console.log(parentElem);

let nodes = [...optionsList.childNodes];
nodes.forEach(items => {console.log(items.nodeType); console.log(items.nodeName)});


let newTestParagraph = document.getElementById("testParagraph");
newTestParagraph.innerHTML = 'This is a paragraph';

let elements = document.querySelector('.main-header').childNodes;
console.log(elements)
    for(let element of elements) {
        if (element.classList) {
            element.classList.add('nav-item');
        }
    }




let removedClasses = [...document.getElementsByClassName('section-title')];
removedClasses.forEach(removedClass => {
    removedClass.classList.remove('section-title')
})
